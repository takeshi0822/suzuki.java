<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>書籍在庫管理システム</title>
</head>
<body>
<h1> ログイン画面 </h1>
<form method="POST" action="<%= request.getContextPath() %>/LoginServlet">
    <table>
        <tr><td>メールアドレス：</td><td><input type="text" name="mailAddress"></td></tr>
        <tr><td>パスワード：</td><td><input type="password" name="password"></td></tr>
        <tr><td><input type="submit" value="ログイン"></td></tr>
    </table>
</form>
<table>
    <tr><td><input type="submit" value="メンバー登録はこちらから" onClick="window.location.href='<%= request.getContextPath() %>/ViewInsertMemberServlet'"></td></tr>
</table>
<font color="red">${ error }</font>
</body>
</html>