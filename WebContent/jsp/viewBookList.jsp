<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>書籍一覧</title>
</head>
<body>
<p><a href="<%= request.getContextPath() %>/LogoutServlet">ログアウト</a>
<h1> 書籍一覧 </h1>
<table>
  <tr>
    <td><c:out value="書籍"/></td>
    <td><c:out value="在庫数"/></td>
  </tr>
  <c:forEach var="obj" items="${bookList}">
    <tr>
      <td>
        <a href="<%= request.getContextPath() %>/ViewBookDetailServlet?id=${obj.id}"><c:out value="${obj.name}"/></a>
      </td>
      <td><c:out value="${obj.stock}"/></td>
    </tr>
  </c:forEach>
</table>
</body>
</html>