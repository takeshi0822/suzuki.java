<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>メンバー登録画面</title>
</head>
<body>
 <h1>メンバー登録画面</h1>
<form method="POST" action="<%= request.getContextPath() %>/InsertMemberServlet">
    <table>
    	<tr><td>氏名：</td><td><input type="text" name="name"></td></tr>
        <tr><td>メールアドレス：</td><td><input type="text" name="mailAddress"></td></tr>
        <tr><td>パスワード：</td><td><input type="password" name="password"></td></tr>
        <tr><td><input type="submit" value="登録"></td></tr>
    </table>
</form>
</body>
</html>