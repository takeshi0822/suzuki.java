<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> 書籍在庫数変更画面 </title>
</head>
<body>
<p><a href="<%= request.getContextPath() %>/LogoutServlet">ログアウト</a>
<h1> 書籍在庫数変更画面 </h1>
<table>
  <tr>
    <th nowrap align="left">書籍名</th><td><c:out value="${book.name}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">著者</th><td><c:out value="${book.author}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">出版社</th><td><c:out value="${book.publisher}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">価格</th><td><c:out value="${book.price}"/>円</td>
  </tr>
  <tr>
    <th nowrap align="left">ISBNコード</th><td><c:out value="${book.isbncode}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">発売日</th><td><c:out value="${book.saledate}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">説明</th><td><c:out value="${book.explanation}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">画像</th><td><img src= "${book.image}"/></td>
  </tr>
  <tr>
    <th nowrap align="left">在庫数</th><td>
      <form action="<%= request.getContextPath() %>/UpdateStockServlet" method="post">
        <input type="text" name="stock" value="${book.stock}">
        <input type="hidden" name="id" value="${book.id}">
        <input type="submit" value="更新">
      </form>
    </td>
  </tr>
</table>
</body>
</html>