package updatestock;

import common.Book;
import common.BookDao;

public class UpdateStockLogic {
	public static void execute(int id, int stock){
		Book book = BookDao.load(id);
		book.setStock(stock);
		BookDao.update(book);
	}
}
