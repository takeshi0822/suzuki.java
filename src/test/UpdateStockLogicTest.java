package test;

import updatestock.UpdateStockLogic;
import viewbookdetail.ViewBookDetailLogic;

import common.Book;

public class UpdateStockLogicTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		UpdateStockLogic.execute(1, 0);
		Book book = ViewBookDetailLogic.execute(1);
		System.out.println("id = " + book.getId());
		System.out.println("stock = " + book.getStock());

	}

}
