package test;

import login.LoginLogic;

import common.Member;

public class LoginLogicTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//存在するユーザ
		Member Member = null;
		Member =  LoginLogic.execute("aqured@aqured.co.jp", "aqured");
		if (Member != null){
			System.out.println("id = " + Member.getId());
			System.out.println("name = " + Member.getName());
			System.out.println("password = " + Member.getPassword());
			System.out.println("mailAddress = " + Member.getMailAddress());
		}else{
			System.out.println("メールアドレスかパスワードが違います");
		}

		System.out.println("------------------------------------");

		//存在しないユーザ
		Member = LoginLogic.execute("unknown@aqured.co.jp","unknown");
		if (Member != null){
			System.out.println("id = " + Member.getId());
			System.out.println("name = " + Member.getName());
			System.out.println("password = " + Member.getPassword());
			System.out.println("mailAddress = " + Member.getMailAddress());
		}else{
			System.out.println("メールアドレスかパスワードが違います");
		}

	}

}
