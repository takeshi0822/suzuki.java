package viewbookdetail;

import common.Book;
import common.BookDao;

public class ViewBookDetailLogic {
	public static Book execute(int id){
		return BookDao.load(id);
	}

}
