package insertmember;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Member;

/**
 * Servlet implementation class InsertMemberServlet
 */
@WebServlet("/InsertMemberServlet")
public class InsertMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertMemberServlet() {
        super();

    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		 String name = request.getParameter("name");
		 String mailAddress = request.getParameter("mailAddress");
		 String password = request.getParameter("password");
		 Member member = new Member(-1, name, mailAddress, password);
		InsertMemberLogic.execute(member);

		request.getRequestDispatcher("/ViewLoginServlet").forward(request, response);

	}

}
