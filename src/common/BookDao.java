package common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class BookDao {
	public static ArrayList<Book> findAll()
	{
		Connection con = null;
		ArrayList<Book> bookList = new ArrayList<Book>();
		try
		{
			con = DBManager.createConnection();
			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM books ORDER BY id";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				bookList.add(
						new Book(rs.getInt("id"), rs.getString("name"), rs.getString("author")
								,rs.getString("publisher"), rs.getInt("price"),rs.getString("isbncode")
								,rs.getDate("saledate"), rs.getString("explanation"), rs.getString("image")
								,rs.getInt("stock")));
			}
			return bookList;

		}catch(Exception ex){
            throw new RuntimeException();
		}finally{
            DBManager.closeConnection(con);
		}
	}


	public static Book load(int id){
		PreparedStatement ps = null;
		Connection con = null;
		try
		{
			con = DBManager.createConnection();

			String sql = "SELECT * FROM books WHERE id = ? ";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			if(rs.next())
			{
				return new Book(rs.getInt("id"), rs.getString("name"), rs.getString("author")
						,rs.getString("publisher"), rs.getInt("price"),rs.getString("isbncode")
						,rs.getDate("saledate"), rs.getString("explanation"), rs.getString("image")
						,rs.getInt("stock"));
			}else{
				return null;
			}
		}catch(Exception ex){
            throw new RuntimeException();
		}finally{
            DBManager.closeConnection(con);
		}
	}

	public static void update(Book book){
		PreparedStatement ps = null;
		Connection con = null;
		try
		{
			con = DBManager.createConnection();
			con.setAutoCommit(false);  //オートコミットはオフ

			String sql = "UPDATE books SET name = ?, author = ?, publisher = ?";
			sql +=", price = ?, isbncode = ?, saledate = ?, explanation = ?, stock = ?";
			sql +="WHERE id = ?";
	        ps = con.prepareStatement(sql);
	        ps.setString(1, book.getName());
	        ps.setString(2, book.getAuthor());
	        ps.setString(3, book.getPublisher());
	        ps.setInt(4, book.getPrice());
	        ps.setString(5, book.getIsbncode());
	        ps.setDate(6,  book.getSaledate());
	        ps.setString(7, book.getExplanation());
	        ps.setInt(8, book.getStock());
	        ps.setInt(9, book.getId());
	        ps.executeUpdate();
	        con.commit();
		}catch(Exception ex){
            throw new RuntimeException();
		}finally{
            DBManager.closeConnection(con);
		}
	}
}
