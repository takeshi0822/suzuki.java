package common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MemberDao {
	public static void insert(Member member){
		PreparedStatement ps = null;
		Connection con = null;
		try
		{
			con = DBManager.createConnection();
			con.setAutoCommit(false);

			String sql = "INSERT INTO members(name,mail_address,password) values(?, ?, ?)";
	        ps = con.prepareStatement(sql);
	        ps.setString(1, member.getName());
	        ps.setString(2, member.getMailAddress());
	        ps.setString(3, member.getPassword());
	        ps.executeUpdate();
	        con.commit();
		}catch(Exception ex){
            throw new RuntimeException();
		}finally{
            DBManager.closeConnection(con);
		}
	}


	public static Member findByMailAddressAndPassword(String mailAddress, String password)
	{
		Connection con = null;
		PreparedStatement ps = null;
		try
		{
			con = DBManager.createConnection();

			String sql = "SELECT * FROM members WHERE mail_address = ? AND password = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, mailAddress);
            ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				return new Member(rs.getInt("id"), rs.getString("name"), rs.getString("mail_address"), rs.getString("password"));
			}else{
				return null;
			}
		}catch(Exception ex){
            throw new RuntimeException();
		}finally{
            DBManager.closeConnection(con);
		}
	}
}
