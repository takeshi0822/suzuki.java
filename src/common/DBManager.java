package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBManager {

	private final static String dbName = "student";
	private final static String url = "jdbc:postgresql://localhost:5432/" + dbName;
	//private final static String url = "jdbc:postgresql:public";
	private final static String userName = "postgres";
	private final static String password = "postgres";

	public static Connection createConnection(){
		try{
			Class.forName("org.postgresql.Driver");
			Connection con=DriverManager.getConnection(url,userName,password);
			return con;
		}catch(Exception e){
			throw new IllegalStateException(e);
		}
	}

	public static void closeConnection(Connection con){
		try{
			con.close();
		}catch(SQLException e){
		}
	}
}
