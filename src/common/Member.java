package common;

public class Member {
	int id;
	String name;
	String mailAddress;
	String password;

	public Member()
	{
	}
	public Member(int id, String name, String mailAddress, String password)
	{
		setId(id);
		setName(name);
		setMailAddress(mailAddress);
		setPassword(password);
	}


	public int getId(){
		return id;
	}
    public void setId(int id){
        this.id = id;
    }
	public String getName(){
		return name;
	}
    public void setName(String name){
        this.name = name;
    }
	public String getMailAddress(){
		return mailAddress;
	}
    public void setMailAddress(String mailAddress){
        this.mailAddress = mailAddress;
    }
	public String getPassword(){
		return password;
	}
    public void setPassword(String password){
        this.password = password;
    }
}
