package common;

import java.sql.Date;

public class Book {
	int id;
	String name;
	String author ;
	String publisher ;
	int price ;
	String isbncode ;
	Date  saledate  ;
	String explanation  ;
	String image  ;
	int stock  ;

	Book(){

	}
	

	Book(int id,String name,String author,String publisher,int price,String isbncode,
			Date saledate, String explanation, String image, int stock){
		setId(id);
		setName(name);
		setAuthor(author);
		setPublisher(publisher);
		setPrice(price);
		setIsbncode(isbncode);
		setSaledate(saledate);
		setExplanation(explanation);
		setImage(image);
		setStock(stock);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getIsbncode() {
		return isbncode;
	}

	public void setIsbncode(String isbncode) {
		this.isbncode = isbncode;
	}

	public Date getSaledate() {
		return saledate;
	}

	public void setSaledate(Date saledate) {
		this.saledate = saledate;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}



}
