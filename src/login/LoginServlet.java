package login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import common.Member;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String mailAddress = request.getParameter("mailAddress");
		 String password = request.getParameter("password");
		 Member member = LoginLogic.execute(mailAddress, password);

		 if(member == null)
		 {
			 if(mailAddress != null || password != null)
			 {
				 request.setAttribute("error", "メールアドレスかパスワードが違います");
			 }
			 request.getRequestDispatcher("/ViewLoginServlet").forward(request, response);

		 }else{
			 HttpSession session = request.getSession(true);
			 session.setAttribute("member", member);
			 request.getRequestDispatcher("/ViewBookListServlet").forward(request, response);
		 }

	}

}
