package login;

import common.Member;
import common.MemberDao;

public class LoginLogic {
	public static Member execute(String mailAdress, String password){
		return MemberDao.findByMailAddressAndPassword(mailAdress, password);
	}

}
